const webpack = require('webpack');
const path = require('path');

const production = process.env.NODE_ENV === 'production';
const appEntry = [path.join(__dirname, 'app/scripts/index.js')];

const plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    // new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|fr|hu/)
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.ProvidePlugin({
        // Promise: 'exports?global.Promise!es6-promise', // using corejs polly fill from cdn
        // 'fetch': 'exports?self.fetch!whatwg-fetch',
    }),
];

if (production) {
    // appEntry.unshift('babel-polyfill');
    plugins.push(

        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.MinChunkSizePlugin({
            minChunkSize: 51200, // ~50kb
        }),
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            output: {
                comments: false,
            },
            compress: {
                warnings: true,
                drop_debugger: false,
            },
        }),
        new webpack.DefinePlugin({
            __SERVER__: !production,
            __DEVELOPMENT__: !production,
            __DEVTOOLS__: !production,
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
                BABEL_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        })
    );
} else {
    appEntry.unshift('webpack/hot/dev-server', 'webpack-hot-middleware/client');
    plugins.push(
        new webpack.HotModuleReplacementPlugin()
    );
}

module.exports = {
    debug: !production,
    // profile: true,
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),

    entry: {
        'mci-training-library': appEntry,
    },

    resolve: {
        root: [path.join(__dirname, 'app')],
        alias: {
            styles: path.join(__dirname, 'app/styles'),
            images: path.join(__dirname, 'app/images'),
            components: path.join(__dirname, 'app/components'),
            templates: path.join(__dirname, 'app/templates'),
            bower: path.join(__dirname, 'bower_components'),
            data: path.join(__dirname, 'data'),
            underscore: path.join(__dirname, 'node_modules/lodash/core'),
        },
    },

    output: {
        path: path.join(__dirname, 'dist/'),
        // filename: production ? 'scripts/[name]-[hash].js' : 'scripts/[name].js',
        filename: 'scripts/[name].js',
        // chunkFilename: production ? 'scripts/[name]-[chunkhash].js' : 'scripts/[name].js',
        chunkFilename: 'scripts/[name].js',
        publicPath: '',
    },

    plugins,

    module: {
        debug: !production,
        devtool: production ? false : 'eval-source-map',
        loaders: [
            {
                test: /\.js$/,
                loaders: ['monkey-hot', 'babel'],
                include: path.join(__dirname, '/app/'),
            },
            {
                test: /\.hbs$/,
                loaders: ['monkey-hot', 'handlebars'],
                include: path.join(__dirname, '/app/'),
            },
        ],
    },
};
