import $ from 'jquery';

const createGUID = function createGUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

export default function formatLinks(value, options) {
    const noLineReturns = value.Links.replace(/(?:\r\n|\r|\n|\u200B)/g, '');
    const $value = $(`<div>${noLineReturns}</div>`);
    const links = $value.find('a');

    // eslint-disable-next-line max-len
    const params = `?QuizName=${value.Quiz_x0020_Name}&CourseName=${value.Title}&StudentID=${window.__gCurrentUser.get_title()}&StudentEmail=${window.__gCurrentUser.get_email()}&SessionGUID=`;

    links.each((index, element) => {
        const $element = $(element);

        if (!!$element.attr('href')) {
            if ($element.attr('href').indexOf('quiz.html') !== -1) {
                $element.attr('href', `/DevPages/LCDisplay3.aspx${params}${createGUID()}`);
            } else {
                const title = value.Title.replace('\'', '\\\'');
                // eslint-disable-next-line max-len
                $element.attr('onclick', `window.updateCourseAccessLog('${window.__gCurrentUser.get_title()}', '${window.__gCurrentUser.get_email()}', '${title}', '${value.Course_x0020_Type}')`);
            }
            $element.attr('target', '_blank');
        }
    });

    return $value.html();
}
