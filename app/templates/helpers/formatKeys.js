export default function formatKeys(value, options) {
    const floatValue = parseFloat(value, 10) || 0;
    return floatValue.toFixed(0);
}
