export default function formatKeys(value, options) {
    if (value) {
        return value
            .map(item => item.lookupValue)
            .sort((a, b) => {
                if (a.toLowerCase() < b.toLowerCase()) {
                    return -1;
                }
                if (a.toLowerCase() > b.toLowerCase()) {
                    return 1;
                }
                return 0;
            })
            .join(', ');
    }
    return '';
}
