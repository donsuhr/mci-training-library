export default function formatGroup(value, options) {
    if (value) {
        return value.join(', ');
    }
    return '';
}
