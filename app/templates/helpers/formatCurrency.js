export default function formatCurrency(value, options) {
    const floatValue = parseFloat(value) || 0;
    let ret;
    if (floatValue === 0) {
        ret = '<span class="free">Free</span>';
    } else {
        const fixedVal = floatValue.toFixed(2);
        const reg = /(\d*)\.(\d*)/;
        const [, dollars, cents] = reg.exec(fixedVal.toString());
        ret = `$${dollars}`;
        if (parseInt(cents, 10) !== 0) {
            ret += `.${cents}`;
        }
    }
    return ret;
}
