import Backbone from 'backbone';

export default Backbone.Router.extend({
    routes: {
        '': 'default',
        'filter(/view/:view)(/filter/:filter)/(:new)': 'filter',
    },

    setState(filter) {
        const view = filter.get('view');
        let filterText = filter.get('filter');
        if (filterText === '') {
            filterText = 'all';
        }
        const New = filter.get('New') ? 'New' : '';
        const url = `filter/view/${view}/filter/${filterText}/${New}`;
        this.navigate(url, { trigger: false });
    },

});
