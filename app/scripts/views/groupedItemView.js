import Backbone from 'backbone';
import templateProduct from 'templates/group-view--groupedByProduct.hbs';
import templateLevel from 'templates/group-view--groupedByLevel.hbs';
import templateType from 'templates/group-view--groupedByType.hbs';
import ItemView from './itemView';
import $ from 'jquery';

const superFishOptions = {
    cssArrows: false,
    speed: 0,
    speedOut: 0,
    delay: 0,
    disableHI: true,
};

const templates = {
    Group: templateProduct,
    Levels: templateLevel,
    Tracks: templateType,
};

export default Backbone.View.extend({
    initialize(options) {
        this.filter = options.filter;
        // first load
        this.listenTo(this.model.get('items'), 'sort', this.render);
    },

    events: {
        'click th': 'onClick',
        'click th button': 'onSortButtonClick',
    },

    onClick(event) {
        const collection = this.model.get('items');
        collection.updateComparator($(event.target).closest('th').data('sort'));
    },

    onSortButtonClick(event) {
        event.preventDefault();
        event.stopPropagation();
        const direction = $(event.target).data('direction') === 'asc' ? 'asc' : 'dec';
        const collection = this.model.get('items');
        collection.updateComparator($(event.target).closest('th').data('sort'), direction);
    },

    render() {
        this.$el.find('.training-library-sf-menu').superfish('destroy');
        const template = templates[this.filter.get('view')] || templates.Group;
        this.$el.html(template(this.model.toJSON()));
        this.$el.find('.training-library-sf-menu').superfish(superFishOptions);

        const target = this.$el.find('tbody');
        const items = this.model.get('items');
        items.each(item => {
            const view = new ItemView({ model: item, filter: this.filter });
            target.append(view.render().el);
        });

        window.globalPageJquery(document).unbind('.fb');
        window.globalPageJquery('.training-library-item__title').fancybox({
            width: 600,
            height: 500,
            hideOnContentClick: false,
            hideOnOverlayClick: true,
            autoDimensions: false,
        });
        window.globalPageJquery('#fancybox-wrap').unbind('mousewheel.fb');

        return this;
    },

});
