import Backbone from 'backbone';
import GroupedItemView from './groupedItemView';

const groupByFunctionNames = {
    Group: 'groupByProduct',
    Levels: 'groupByLevels',
    Tracks: 'groupByType',
};

export default Backbone.View.extend({
    tagName: 'div',
    initialize() {
        // first load
        this.listenTo(this.model.get('filteredCollection'), 'sync reset', this.render);
        // set by filter update
        // use custom because changing view does not trigger a change
        this.listenTo(this.model, 'fillerSet', this.render);
    },

    render() {
        this.$el.html('');
        const filteredCollection = this.model.get('filteredCollection');
        const currentFilterView = this.model.get('filter').get('view');
        const groupFunction = groupByFunctionNames[currentFilterView] || groupByFunctionNames.Group;
        const groupedCollection = filteredCollection[groupFunction](this.model.get('filter'));

        groupedCollection.each(item => {
            const view = new GroupedItemView({ model: item, filter: this.model.get('filter') });
            this.$el.append(view.el);
            view.render();
        });

        return this;
    },

});
