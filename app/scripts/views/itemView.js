import Backbone from 'backbone';
import templateProduct from 'templates/item-view--product.hbs';
import templateLevel from 'templates/item-view--level.hbs';
import templateType from 'templates/item-view--type.hbs';

const templates = {
    Group: templateProduct,
    Levels: templateLevel,
    Tracks: templateType,
};

export default Backbone.View.extend({
    tagName: 'tr',
    initialize(options) {
        this.filter = options.filter;
    },

    render() {
        const template = templates[this.filter.get('view')] || templates.Group;
        this.$el.html(template(this.model.toJSON()));
        return this;
    },
});
