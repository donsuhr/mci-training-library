import Backbone from 'backbone';
import _ from 'lodash';

module.exports = Backbone.View.extend({
    tagName: 'div',
    className: 'loading',
    initialize(options) {
        this.$container = options.$container;
        this.offset = _.extend({}, { left: 0, top: 0 }, options.offset);
        this.render();
    },

    render() {
        this.$el.text('Loading...');
        return this;
    },

    show() {
        this.$container.append(this.$el);
        const x = (this.$container.innerWidth() / 2 - this.$el.innerWidth() / 2) + this.offset.left;
        const y = Math.max(0,
            (this.$container.innerHeight() / 2 - this.$el.innerHeight() / 2) + this.offset.top
        );
        this.$el.css({ left: x, top: y });
    },

    hide() {
        this.remove();
    },

});
