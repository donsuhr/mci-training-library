import $ from 'jquery';
import Backbone from 'backbone';
import LoadingView from './loadingView';
import _ from 'lodash';
import ListCollection from '../listCollection';
import Filter from '../filter';
import GroupedListView from './groupedListView';
import MenuView from './menuView';
import DetailView from './detailsView';

const defaults = {
    webURL: '/tlc',
    listName: 'TrainingLibrary v2',
};

export default Backbone.View.extend({
    initialize(options) {
        this.options = _.extend({}, defaults, options);
        this.router = options.router;
        this.filter = new Filter({ router: options.router });
        this.loadingView = new LoadingView({ $container: this.$el });

        this.listCollection = new ListCollection();
        this.selectedFilterModel = new Backbone.Model({
            allItemsCollection: this.listCollection,
            filteredCollection: new ListCollection(),
            filter: this.filter,
        });

        this._load().then(result => {
            this.listCollection.reset(result);
            this.selectedFilterModel.set('allItemsCollection', this.listCollection);
        });

        this.menuView = new MenuView({
            model: this.selectedFilterModel,
        });

        this.groupView = new GroupedListView({
            // el: this.$el.find('.training-library-table tbody'),
            model: this.selectedFilterModel,
        });

        this.detailView = new DetailView({
            model: this.selectedFilterModel,
        });

        this.listenTo(this.filter, 'change', this.filterListView);
        this.listenTo(this.listCollection, 'sync reset', this.onListCollectionSync);
        this.render();
        this.loadingView.show();
    },

    render(event) {
        this.$el.html('');
        this.$el.append(this.detailView.el);
        this.$el.append(this.menuView.el);
        this.$el.append(this.groupView.el);
        this.groupView.render();
    },

    onListCollectionSync(event) {
        this.filterListView();
        this.loadingView.hide();
    },

    filterListView() {
        const filtered = this.listCollection.applyFilter(this.filter);
        this.selectedFilterModel.set('filteredCollection', filtered);
        this.selectedFilterModel.trigger('fillerSet');
    },

    _load() {
        return new Promise((resolve, reject) => {
            // eslint-disable-next-line new-cap
            const getItemsJqueryPromise = $().SPServices.SPGetListItemsJson({
                webURL: this.options.webURL,
                listName: this.options.listName,
                CAMLQuery: `<Query>
                        <Where>
                            <Eq>
                                <FieldRef Name="Active"/>
                                <Value Type="Integer">1</Value>
                            </Eq>
                        </Where>
                        <OrderBy>
                            <FieldRef Name="DisplayOrder" Type="Number" Ascending="TRUE" />
                        </OrderBy>
                    </Query>`,
                CAMLViewFields: '<ViewFields><FieldRef Name="Active"/>' +
                '<FieldRef Name="New"/>' +
                '<FieldRef Name="Description"/>' +
                '<FieldRef Name="ID"/>' +
                '<FieldRef Name="Links"/>' +
                '<FieldRef Name="DisplayOrder"/>' +
                '<FieldRef Name="Levels"/>' +
                '<FieldRef Name="Title"/>' +
                '<FieldRef Name="Course_x0020_Type"/>' +
                '<FieldRef Name="Duration"/>' +
                '<FieldRef Name="Quiz_x0020_Name"/>' +
                '<FieldRef Name="Keys"/>' +
                '<FieldRef Name="Cost"/>' +
                '<FieldRef Name="Tracks"/>' +
                '<FieldRef Name="Group"/></ViewFields>',
                debug: false,
            });

            $.when(getItemsJqueryPromise).done(function cb() {
                resolve(this.data);
            });
        });
    },
});
