/* global __gCurrentUser:true, CurrentUser:true */

import Backbone from 'backbone';
import template from 'templates/details-view.hbs';

export default Backbone.View.extend({
    tagName: 'div',
    className: '',

    initialize() {
        // first load
        this.listenTo(this.model.get('allItemsCollection'), 'sync reset', this.render);

        if (typeof __gCurrentUser === 'undefined' || __gCurrentUser === null) {
            __gCurrentUser = new CurrentUser();
        }
        __gCurrentUser.addCallback(this.render.bind(this));
    },

    render() {
        if (__gCurrentUser !== null && __gCurrentUser.getStatus() === __gCurrentUser.LOAD_SUCCESS) {
            this.$el.html(template({ items: this.model.get('allItemsCollection').toJSON() }));
        }
        return this;
    },
});
