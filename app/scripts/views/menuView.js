import Backbone from 'backbone';
import template from 'templates/menuView.hbs';
import $ from 'jquery';
import _ from 'lodash';

const VIEW_ALL_TEXT = 'View All';

const superFishOptions = {
    cssArrows: false,
    speed: 0,
    speedOut: 0,
    delay: 0,
    disableHI: true,
};

export default Backbone.View.extend({
    tagName: 'nav',

    initialize() {
        this.listenTo(this.model.get('allItemsCollection'), 'sync reset', this.render);
    },

    events: {
        'click .training-library-nav-item li': 'onClick',
        'click .training-library-nav-item--new': 'onNewClick',
    },

    onClick(event) {
        const $target = $(event.target);
        const $menu = $target.closest('.training-library-nav-item');
        const text = $.trim($target.data('title'));
        const property = $menu.data('property');
        // this.setSelectedText($menu, text);
        const $others = this.$el.find('.training-library-nav-item').not($menu);
        $others.each((index, menu) => this.resetMenu(menu));
        this.closeMenu($menu);
        const filterText = text === VIEW_ALL_TEXT ? '' : text;
        this.model.get('filter').setState({
            filter: filterText,
            view: property,
        });
    },

    onNewClick(event) {
        const $others = this.$el.find('.training-library-nav-item');
        $others.each((index, menu) => this.resetMenu(menu));
        this.model.get('filter').setState({
            New: 'New',
            view: 'Group',
            filter: 'all',
        });
    },

    resetMenu(menu) {
        const $menu = $(menu);
        const defaultText = $menu.data('defaultText');
        this.setSelectedText($menu, defaultText);
    },

    setSelectedText($menu, text) {
        const defaultText = $menu.data('defaultText');
        const newText = VIEW_ALL_TEXT === text ? defaultText : text;
        $menu.find('.training-library-nav-item__title').text(newText);
        // this.toggleViewAll($menu, VIEW_ALL_TEXT !== text && text !== defaultText);
    },

    closeMenu($menu) {
        $menu.closest('.sf-js-enabled').superfish('hide');
    },

    toggleViewAll($menu, force) {
        $menu.find('li:last').toggle(force);
    },

    isDefaultText(_text) {
        const text = $.trim(_text);
        return (text === '' || text === VIEW_ALL_TEXT);
    },

    render() {
        const model = this.model.get('allItemsCollection');
        const products = model.getUniqueProducts();
        products.push('View All');
        const levels = _.map(model.getLevels(), 'name');
        levels.push('View All');
        const tracks = model.getTracks();
        tracks.push('View All');
        this.$el.html(template({
            products,
            levels,
            tracks,
        }));
        // const $menus = this.$el.find('.training-library-nav-item');
        // $menus.each((index, menu) => this.resetMenu(menu));
        this.$el.find('.training-library-nav').superfish(superFishOptions);
        return this;
    },

});
