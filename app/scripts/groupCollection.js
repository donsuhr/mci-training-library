import Backbone from 'backbone';

const groupCollection = Backbone.Collection.extend({
    initialize(models, options) {
        this.updateComparator();
    },

    getTrackValues(model) {
        const tracks = model.get('Tracks');
        if (tracks) {
            return tracks
                .map(item => item.lookupValue.toLowerCase())
                .sort()
                .join(', ');
        }
        return '';
    },

    updateComparator(by = '', direction) {
        let sortBy;
        let comparatorFnName;
        const that = this;

        switch (by) {
            case 'Tracks':
                sortBy = function sortByTrack(a, b) {
                    let aVal = that.getTrackValues(a);
                    let bVal = that.getTrackValues(b);
                    if (aVal < bVal) {
                        return -1;
                    }
                    if (aVal > bVal) {
                        return 1;
                    }
                    // return 0;
                    aVal = a.get('Title');// .toLowerCase();
                    bVal = b.get('Title');// .toLowerCase();
                    if (aVal < bVal) {
                        return -1;
                    }
                    if (aVal > bVal) {
                        return 1;
                    }
                    return 0;
                };
                comparatorFnName = 'sortByTrack';
                break;
            case 'Group':
                sortBy = function sortByGroup(item) {
                    return [item.get('Group'), item.get('Title')];
                };
                comparatorFnName = 'sortByGroup';
                break;
            case 'Levels':
                sortBy = function sortByLevel(item) {
                    // return parseInt(item.get('Levels'), 10) || 0;
                    return [item.get('Levels'), item.get('Title')];
                };
                comparatorFnName = 'sortByLevel';
                break;
            default:
            case 'Title':
                sortBy = function sortByTitle(item) {
                    return item.get('Title');
                };
                comparatorFnName = 'sortByTitle';
                break;
        }
        const directionIsSet = typeof direction !== 'undefined';
        const sameComparator = this.comparator && this.comparatorFnName === comparatorFnName;

        if ((!directionIsSet && sameComparator) || (directionIsSet && direction === 'dec')) {
            sortBy = this.reverseSortBy(sortBy);
            comparatorFnName = `${comparatorFnName}-reversed`;
        }
        this.comparator = sortBy;
        this.comparatorFnName = comparatorFnName;
        this.sort();
    },

    reverseSortBy(sortByFunction) {
        if (sortByFunction.length === 2) {
            return function reverseSort(a, b) {
                const sortVal = sortByFunction(a, b);
                return sortVal * -1;
            };
        }
        // http://stackoverflow.com/questions/5013819/reverse-sort-order-with-backbone-js
        return function reverseSort(left, right) {
            const l = sortByFunction(left);
            const r = sortByFunction(right);

            if (l === void 0) {
                return -1;
            }
            if (r === void 0) {
                return 1;
            }
            // eslint-disable-next-line
            return l < r ? 1 : l > r ? -1 : 0;
        };
    },

});

export default groupCollection;
