import Backbone from 'backbone';
import GroupCollection from './groupCollection';

export default Backbone.Model.extend({
    defaults: {
        name: '',
        items: new GroupCollection(),
    },
});
