import Backbone from 'backbone';
import _ from 'lodash';

import GroupCollection from './groupCollection';
import Group from './groupModel';

const ListCollection = Backbone.Collection.extend({
    comparator: 'DisplayOrder',
    initialize(options) {

    },

    applyFilter(filter) {
        let result = this.models;
        let filterVal = filter.get('filter') || '';
        if (filterVal === 'all') {
            filterVal = '';
        }
        switch (filter.get('view')) {
            default:
            case 'Group':
                if (filterVal !== '') {
                    result = _.filter(result,
                        model => _.indexOf(model.get('Group'), filterVal) !== -1
                    );
                }
                if (filter.get('New') === 'New') {
                    result = _.filter(result, model => model.get('New') === true);
                }

                break;
            case 'Levels':
                if (filterVal !== '') {
                    const levelVal = _.find(this.getLevels(), { name: filterVal }).val;
                    result = _.filter(result, model => {
                        const modelVal = parseInt(model.get('Levels'), 10) || 0;
                        const val = Math.floor(modelVal / 100) * 100;
                        return val === levelVal;
                    });
                }
                break;
            case 'Tracks':
                if (filterVal !== '') {
                    result = _.filter(result, model =>
                        _.filter(
                            model.attributes.Tracks,
                            track => track.lookupValue === filterVal
                        ).length
                    );
                }
                break;
        }

        return new ListCollection(result);
    },

    getUniqueProducts() {
        return _.uniq(_.flatten(this.pluck('Group'))).sort();
    },

    groupByProduct(filter) {
        const grouped = {};
        const filterVal = filter && filter.get('filter');
        const useFilter = (filterVal && filterVal !== 'all' && filterVal !== '');

        _.forEach(this.models, model => {
            let groups = _.get(model, 'attributes.Group');
            if (groups.length === 0) {
                groups = ['No Product'];
            }
            _.forEach(groups, group => {
                if (useFilter && group !== filterVal) {
                    return;
                }
                if (!grouped.hasOwnProperty(group)) {
                    grouped[group] = [];
                }
                grouped[group].push(model);
                if (filter.get('New') === 'New') {
                    // eslint-disable-next-line consistent-return
                    return false;
                }
            });
        });

        const result = _.map(grouped,
            (value, index, collection) => new Group({
                name: index,
                items: new GroupCollection(value),
            })
        );

        const comparator = function groupByTypeComparator(item) {
            return item.get('name').toLowerCase();
        };

        return new Backbone.Collection(result, { comparator });
    },

    getLevelNameFromCourseNumber(courseNumber) {
        const num = parseInt(courseNumber, 10) || 0;
        const val = Math.floor(num / 100) * 100;
        return _.find(this.getLevels(), { val }).name;
    },

    getLevels() {
        return [
            { val: 0, name: 'Level 000 Overview' },
            { val: 100, name: 'Level 100 Courses' },
            { val: 200, name: 'Level 200 Courses' },
            { val: 300, name: 'Level 300 Courses' },
            { val: 400, name: 'Level 400 Courses' },
            { val: 700, name: 'Level 700 Releases' },
            { val: 900, name: 'Level 900 Best Practices' },
        ];
    },

    groupByLevels() {
        // eslint-disable-next-line max-len
        const grouped = _.groupBy(this.models, model => this.getLevelNameFromCourseNumber(model.get('Levels')));
        const sorted = _.keys(grouped).sort();
        const result = _.map(sorted,
            group => new Group({
                name: this.getLevelNameFromCourseNumber(grouped[group][0].get('Levels')),
                items: new GroupCollection(grouped[group]),
            })
        );
        return new Backbone.Collection(result);
    },

    getTracks() {
        const filtered = this.models.filter(model => _.get(model, 'attributes.Tracks') !== null);
        const mapped = filtered.map(
            model => model.attributes.Tracks.map(
                track => track.lookupValue
            )
        );
        return _.uniq(_.flatten(mapped)).sort((a, b) => {
            if (a.toLowerCase() < b.toLowerCase()) {
                return -1;
            }
            if (a.toLowerCase() > b.toLowerCase()) {
                return 1;
            }
            return 0;
        });
    },

    groupByType(filter) {
        const grouped = {};
        const filterVal = filter && filter.get('filter');
        const useFilter = filterVal && filterVal !== 'all' && filterVal !== '';

        for (const model of this.models) {
            const tracks = _.get(model, 'attributes.Tracks');
            let flatTracks = _.map(tracks, track => track.lookupValue);
            if (flatTracks.length === 0) {
                flatTracks = ['No Track'];
            }

            flatTracks.forEach(track => {
                if (useFilter && track !== filterVal) {
                    return;
                }
                if (!grouped.hasOwnProperty(track)) {
                    grouped[track] = [];
                }
                grouped[track].push(model);
            });
        }

        const result = _.map(grouped,
            (value, index, collection) => new Group({
                name: index,
                items: new GroupCollection(value),
            })
        );

        const comparator = function groupByTypeComparator(item) {
            return item.get('name').toLowerCase();
        };

        return new Backbone.Collection(result, { comparator });
    },
});

export default ListCollection;
