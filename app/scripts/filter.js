import Backbone from 'backbone';
import _ from 'lodash';

export default Backbone.Model.extend({
    initialize(options) {
        this.router = options.router;
        this.listenTo(this.router, 'route:filter', this.onRoute);
        this.listenTo(this.router, 'route:default', this.onDefaultRoute);
    },

    defaults: {
        view: 'Group',
        filter: 'all',
        New: null,
    },

    onDefaultRoute() {
        this.setState(this.defaults);
    },

    onRoute(view = 'Group', filter = 'all', New = null) {
        this.setState({
            view,
            filter,
            New,
        });
    },

    setState(options) {
        this.clear({ silent: true });
        _.map(options, (_value, key) => {
            const value = _.isString(_value) ? _.trim(_value) : _value;
            this.set(key, value, { silent: true });
        });
        if (!_.isEqual(options, this.defaults)) {
            this.router.setState(this);
        }
        this.trigger('change', this);
    },
});
