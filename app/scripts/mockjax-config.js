import $ from 'jquery';
import websText from 'raw!data/Webs.xml';
import listText from 'raw!data/List.xml';
require('jquery-mockjax')($, window);

$.mockjax({
    url: '/_vti_bin/Webs.asmx',
    dataType: 'xml',
    contentType: 'text/xml',
    // responseTime: 1150,
    responseXML: websText,
});

$.mockjax({
    url: '/tlc/_vti_bin/Lists.asmx',
    dataType: 'xml',
    contentType: 'text/xml',
    // responseTime: 2150,
    responseXML: listText,
});
