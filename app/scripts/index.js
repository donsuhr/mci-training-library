/* global process */

const globalPageJquery = window.jQuery;
window.globalPageJquery = globalPageJquery;

import $ from 'jquery';
// require('babel-polyfill');

// recycle global window jQuery -----------------------
window.jQuery = window.$ = $;

import Backbone from 'backbone';
Backbone.$ = $;
require('bower/SPServices/src/jquery.SPServices.js');
require('superfish');

if (process.env.NODE_ENV !== 'production') {
    require('./mockjax-config');
}

window.updateCourseAccessLog = function updateCourseAccessLog(username, email, course, courseType) {
    // eslint-disable-next-line new-cap
    $().SPServices({
        operation: 'UpdateListItems',
        async: true,
        batchCmd: 'New',
        listName: 'CourseAccessLog',
        valuepairs: [
            ['Title', username],
            ['UserEmail', email],
            ['CourseName', course],
            ['CourseType', courseType],
        ],
    });
    return true;
};

window.jQuery = window.$ = globalPageJquery;
// end recycle window jQuery -----------------------

import AppView from './views/appView';
import Router from './router';

const el = document.getElementById('trainingLibrary');
const router = new Router();

// eslint-disable-next-line no-new
new AppView({
    el,
    router,
});
// var router = new Router();
Backbone.history.start();
