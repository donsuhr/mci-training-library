module.exports = function svgmin(grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    cwd: `${options.app}/images`,
                    src: '{,*/}*.svg',
                    dest: options.distImages,
                },
            ],
        },

    };
};
