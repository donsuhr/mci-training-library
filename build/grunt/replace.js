module.exports = function replace(grunt, options) {
    return {
        cssImgOne: {
            src: [`${options.dist}/styles/**/*.css`],
            overwrite: true,
            replacements: [
                {
                    // eslint-disable-next-line no-useless-escape
                    from: '\/images\/',
                    to: '/SiteCollectionImages/',
                },
            ],
        },
    };
};
