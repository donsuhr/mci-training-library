module.exports = function imagemin(grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    cwd: `${options.app}/images`,
                    src: '**/*.{gif,jpeg,jpg,png}',
                    dest: options.distImages,
                },
            ],
        },
    };
};
