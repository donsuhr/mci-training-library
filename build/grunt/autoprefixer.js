module.exports = function autoprefixer(grunt, options) {
    return {
        options: {
            browsers: ['> 1%', 'last 3 versions', 'Firefox ESR', 'Opera 12.1'],
            map: {
                inline: false,
            },
        },
        dist: {
            files: [
                {
                    expand: true,
                    cwd: options.sassOutput, // '<%= compass.options.cssDir %>',
                    src: '**/*.css',
                    dest: '.tmp/styles/',
                },
            ],
        },
    };
};
