module.exports = function copy(grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: options.app,
                    dest: options.dist,
                    src: [
                        '*.{ico,png,txt}',
                        'images/{,*/}*.webp',
                        'fonts/{,*/}*.*',
                    ],
                },
                {
                    expand: true,
                    dot: true,
                    cwd: '.tmp/images',
                    src: ['*.svg', '*.png'],
                    dest: `${options.dist}/images`,
                },
            ],
        },
        html: {
            // dist is moving with replace
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: '.tmp',
                    src: ['**/*.html'],
                    dest: options.dist,
                },
            ],
        },
    };
};
