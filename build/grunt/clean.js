module.exports = function clean(grunt, options) {
    return {
        dev: {
            files: [{
                dot: true,
                src: ['.tmp'],
            }],
        },
        dist: {
            files: [{
                dot: true,
                src: [
                    `${options.dist}/*`,
                    `!${options.dist}/.git*`,
                ],
            }],
        },
    };
};
