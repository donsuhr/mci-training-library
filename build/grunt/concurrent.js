module.exports = function concurrent(grunt, options) {
    return {
        options: {
            logConcurrentOutput: true,
            limit: 8,
        },
        dist: [
            'sass',
            'imagemin',
            'svgmin',
            'copy:dist',
            'webpack:dist',
        ],
        serveSupport: [
            'watch:sass',
            'watch:js',
            'watch:livereload',
        ],
        serveSupportWithLint: [
            'watch:sassWithLint',
            'watch:js',
            'watch:livereload',
        ],
    };
};
