module.exports = function gruntfile(grunt) {
    const config = {
        app: 'app',
        dist: 'dist',
        distImages: 'dist/SiteCollectionImages/tlc/',
        sassOutput: '.tmp/sass-out/',
        svgSpriteOutput: '.tmp/iconizr',
        requireJsOutput: '.require-build',
        livereloadPort: 35731,
        testLiveReload: 35732,
    };

    require('time-grunt')(grunt);

    require('load-grunt-config')(grunt, {
        configPath: require('path').join(process.cwd(), '/build/grunt'),
        data: config,
        jitGrunt: {
            staticMappings: {
                sprite: 'grunt-spritesmith',
                useminPrepare: 'grunt-usemin',
                scsslint: 'grunt-scss-lint',
                replace: 'grunt-text-replace',
                newer: 'grunt-newer',
                svgsprite: 'grunt-svg-sprite', // never worked
            },
        },
    });

    grunt.registerTask('serve', [
        'sass',
        'autoprefixer',
        'connect:livereload',
        'concurrent:serveSupport',
    ]);

    grunt.registerTask('serveWithLint',
        [
            'build',
            'connect:livereload',
            'concurrent:serveSupportWithLint',
        ]
    );

    grunt.registerTask('build', [
        'clean:dist',
        'concurrent:dist',
        'autoprefixer',
        'cssmin',
        'replace',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);
};
